const Sequelize = require('sequelize');
const env = require('./env');

const sequelize = new Sequelize(env.database,env.username,env.password,{
    host:env.host,
    dialect:env.dialect,
    pool:env.pool
});

const db ={}
db.Sequelize = Sequelize;
db.sequelize = sequelize;

//! Inintialize model
db.usertables =require('../models/user.model')(sequelize, Sequelize);
module.exports = db ;