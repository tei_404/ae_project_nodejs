module.exports = (sequelize, Sequelize) => {
    //! " demos " is table name
    const Users = sequelize.define('usertables',
        { 
            //! ID , Username ,idmanage , manage ,tel , address
            id:{
                type: Sequelize.INTEGER, 
                field:'id',
                primaryKey: true,
                autoIncrement: true

            },
            username: {
                type: Sequelize.STRING,
                field: 'name'

            },
            iddepartment: {
                type: Sequelize.STRING,
                field: 'iddepartment'

            },
            department: {
                type: Sequelize.STRING,
                field: 'department'

            },
            tell: {
                type: Sequelize.STRING,
                field: 'tell'

            },
            address: {
                type: Sequelize.STRING,
                field: 'address'

            },
            device: {
                type: Sequelize.STRING,
                field: 'device'

            }
            
        },{
        timestamps: true
    });
    return Users;
}