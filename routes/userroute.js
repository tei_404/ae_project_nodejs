const express = require('express');
const route = express.Router();
const path = require('path');
const db = require('../utils/db.config');
//! call table name to " Users "
const Users = db.usertables;

//! Define  sequelize
const sequelize = db.sequelize;
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


route.get('/', async (req, res) => {
    // res.status(200).json({ status: 200, message: 'Route Sucessecfull' })
    res.sendFile(path.join(__dirname, '../src/index.html'));
})

//! save file to db
route.post('/', async (req, res) => {
    let username = req.body.username;
    console.log(username)

    let saved = null;
    if (req.body) {
        saved = await sequelize.transaction(function (t) {
            return Users.create(req.body, { transaction: t });
        })
    }
    if (saved != null) {
        res.status(200).json({ status: 200, data: saved });
    } else {
        res.status(201).json({ status: 201, message: 'Error' });
    }

})

//! find all data
route.get('/findall', async (req, res) => {
    res.status(200).json({ status: 200, data: await Users.findAll() })
})

//! find data by pagenation
route.get('/pagenation/:page', async (req, res) => {
    let limit = 12;
    let page = req.params.page - 1
    let offset = page * limit 
    await Users.findAndCountAll({
        limit: limit,
        offset: offset,
        order: [['id', 'ASC']]
    }).then((data) => {
        console.log(data)
        let pages = Math.ceil(data.count / limit);
        offset = limit * (page - 1)
        let ihave = data.rows;
        res.status(200).json({ 'result': ihave, 'count': data.count, 'pages': pages })
      

    })
})

//! number id tital
//   $.get('/dashboard/findallcount', function (res) {
//     console.log(res.data);
//     document.getElementById("numbertotla").innerHTML= ""+ res.data;

// })
//! find by iddepartment
route.get('/findbyiddepartment/:iddepartment', async (req, res) => {
    let iddepartment = req.params.iddepartment;
    let data = await Users.findAll({ where: { iddepartment: iddepartment } });
    res.status(200).json({ status: 200, data: data })
})
//! find by id
route.get('/findbyid/:id', async (req, res) => {
    let id = req.params.id;
    let data = await Users.findAll({ where: { id: id } });
    res.status(200).json({ status: 200, data: data })
})

//! delete by id
route.delete('/delete/:id', async (req, res) => {
    let id = req.params.id;
    let deleted = await sequelize.transaction(function (t) {
        return Users.destroy({ where: { id: id }, transaction: t })
    })
    res.status(200).json(deleted)
})

//! update by id
route.patch('/update/:id', async (req, res) => {
    let id = req.params.id;
    let body = req.body;
    let update = await sequelize.transaction(function (t) {
        return Users.update(body, { where: { id: id }, transaction: t })
    })
    res.status(200).json(update)
})

module.exports = route;