const express = require('express');
const path =require('path');
const app = express();
const LoggerMiddleware =(req ,res ,next)=>{
    console.log(`Logger ${req.url} ${res.method} -- ${new Date()}`);
    next();
}

app.use(LoggerMiddleware);
app.use(express.json());

//!set middle path  
app.use('/js',express.static(path.join(__dirname,'../src/js')))
app.use('/css',express.static(path.join(__dirname,'../src/css')))

 //! Routes
 const loginRoute = require('../routes/loginroute');
 app.use('/', loginRoute);
//  app.use('/login', loginRoute);

 const userRoute = require('../routes/userroute');
 app.use('/user', userRoute);
 

module.exports = app ;